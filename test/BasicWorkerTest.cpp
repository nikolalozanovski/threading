#include <catch_amalgamated.hpp>

#include <string>

#include <Worker.hpp>
#include <BasicWorker.hpp>

const std::string CLASS_NAME {"[BasicWorker]"};


TEST_CASE("basic-worker", CLASS_NAME) {
    int value = 0;
    auto increment = [&] {
        if (value == 100) {
            return;
        }

        value++;
    };
    threading::BasicWorker worker(increment);
    // worker not started
    REQUIRE(value == 0);
    worker.work();

    while (value < 100) {
        std::this_thread::sleep_for(std::chrono::nanoseconds(10));
    }

    worker.stop();

    if (worker.finishable()) {
        worker.waitWorkerToFinish();
    }

    REQUIRE(value == 100);
}

TEST_CASE("worker-wait", CLASS_NAME) {
    bool done;
    auto work = [&] {
        done = true;
    };
    std::unique_ptr<threading::Worker> worker = std::make_unique<threading::BasicWorker>(work);
    worker->work();
    auto finishedWaitingTheTask {worker->waitToCompleteTask()};
    worker->stop();
    worker->waitWorkerToFinish();
    REQUIRE(done == true);
    REQUIRE(finishedWaitingTheTask == true);
}
