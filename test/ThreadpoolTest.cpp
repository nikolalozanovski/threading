#include <catch_amalgamated.hpp>

#include <string>
#include <iostream>
#include <atomic>

#include <Threadpool.hpp>

const std::string CLASS_NAME {"[Threadpool]"};

struct Result {
    Result(int result) : result{result} {}
    int result;
};

int calculate(int i, int j, int k) {
    return (i + j) * k;
}

Result calculateTheResult(int i, int j, int k) {
    return Result((i + j) * k);
}

struct CustomRunnable : public Runnable {
    int i{0};
    void run() {
        i = 5;
    }
};

TEST_CASE("add-task", CLASS_NAME) {
    threading::Threadpool tp(2);
    bool taskCompletedWithNoErrors;
    auto ob = tp.add([](int i) {}, 88);
    ob->onComplete([&]() {
        taskCompletedWithNoErrors = true;
    });
    tp.waitTasks();
    REQUIRE(taskCompletedWithNoErrors == true);
}

TEST_CASE("threadpool-create", CLASS_NAME) {
    threading::Threadpool tp(2);
    REQUIRE(tp.workerCount() == 2);
}

TEST_CASE("Add-prioritized-task", CLASS_NAME) {
    threading::Threadpool tp(2);
    int theResult;
    auto ob = tp.addPrioritizedTask(
                  threading::Threadpool::TASK_PRIORITY_CRITICAL,
                  calculate, 1, 2, 3);
    ob->onComplete([&theResult](int result) {
        theResult = result;
    });

    tp.waitTasks();
    REQUIRE(theResult == 9);
}

TEST_CASE("wait-for-tasks", CLASS_NAME) {
    threading::Threadpool tp(1);
    uint64_t finishedTasks{0};
    std::mutex mutex;

    for (size_t i {1}; i <= 10; i++) {
        auto ob = tp.add([&]() {});
        ob->onComplete([&]() {
            std::unique_lock ul (mutex);
            finishedTasks++;
        });
    }

    tp.waitTasks();
    REQUIRE(finishedTasks == 10);
}

TEST_CASE("calculate-with-complex-object", CLASS_NAME) {
    threading::Threadpool tp(2);
    int complexResult;
    auto ob1 = tp.add(calculateTheResult, 5, 6, 7);
    ob1->onComplete([&complexResult](Result && result) {
        complexResult = result.result;
    });
    tp.waitTasks();
    REQUIRE(complexResult == 77);
}

TEST_CASE("pause-threadpool-not-executing", CLASS_NAME) {
    threading::Threadpool tp(8);
    tp.pause();

    std::atomic<uint64_t> result{0};
    const uint32_t numberOfTasks{10000};
    auto task = [&] () {
        result++;
    };

    for (uint32_t i = 0 ; i < numberOfTasks; i++) {
        tp.add(task);
    }

    REQUIRE(result.load() == 0);

    tp.resume();
    tp.waitTasks();

    REQUIRE(result.load() == numberOfTasks);
}

TEST_CASE("stop-threadpool-not-executing", CLASS_NAME) {
    threading::Threadpool tp(2);
    tp.pause();

    std::atomic<int> result{0};

    for (uint32_t i = 0 ; i < 1000; i++) {
        tp.add([&result] () {
            result++;
        });
    }

    REQUIRE(result.load() == 0);

    tp.resume();
    std::this_thread::sleep_for(std::chrono::nanoseconds(5));
    tp.stop();
    std::this_thread::sleep_for(std::chrono::nanoseconds(5));

    REQUIRE( (result.load() > 0 and result.load() < 100) );
}

TEST_CASE("resume-after-stop-no-effect", CLASS_NAME) {
    threading::Threadpool tp(2);
    tp.pause();

    std::atomic<int> result{0};

    for (uint32_t i = 0 ; i < 1000; i++) {
        tp.add([&result] () {
            result++;
        });
    }

    REQUIRE(result.load() == 0);

    tp.resume();
    std::this_thread::sleep_for(std::chrono::nanoseconds(5));
    tp.stop();
    std::this_thread::sleep_for(std::chrono::nanoseconds(5));
    auto resultAfterStop {result.load()};

    tp.resume();
    std::this_thread::sleep_for(std::chrono::nanoseconds(10));
    auto resultAfterInEffectiveResume {result.load()};

    REQUIRE(resultAfterStop == resultAfterInEffectiveResume);
}

TEST_CASE("custom-runnable", CLASS_NAME) {
    auto customRunnable = std::make_shared<CustomRunnable>();
    threading::Threadpool tp(2);
    tp.pause();
    tp.addRunnable(customRunnable);
    tp.resume();
    tp.waitTasks();

    REQUIRE(customRunnable->i == 5);
}
