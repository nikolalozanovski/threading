#include <catch_amalgamated.hpp>

#include <Task.hpp>

#include <iostream>

const std::string CLASS_NAME {"[Task]"};

const auto voidFunction = [](){};
const auto nonVoidFunction = [](){
	return 12;
};
const auto complexFunctionWithArgument = [](int arg) {
	return arg + 5;
};
const auto complexVoidFunctionWithArgument = [](int* arg) {
	(*arg) += 5;
};

TEST_CASE("void-task", CLASS_NAME) {
    int value;
    threading::Task task(voidFunction);
    task.onComplete([&] {
        value = 7;
    });
    task.run();
    REQUIRE(value == 7);
}

TEST_CASE("non-void-task", CLASS_NAME) {
	int result;
	threading::Task task(nonVoidFunction);
	task.onComplete([&](int res) {
		result = res;
	});
	task.run();
	REQUIRE(result == 12);
}

TEST_CASE("complex-task-with-argument", CLASS_NAME) {
	int argument = 15;
	int result;
	threading::Task task(complexFunctionWithArgument, argument);
	task.onComplete([&](int res) {
		result = res;
	});
	task.run();
	REQUIRE(result == 20);
}

TEST_CASE("complex-void-task-with-argument", CLASS_NAME) {
	int argument = 15;
	threading::Task task(complexVoidFunctionWithArgument, &argument);
	task.run();
	REQUIRE(argument == 20);
}
