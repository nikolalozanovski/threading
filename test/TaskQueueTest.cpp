#include <catch_amalgamated.hpp>

#include <TaskQueue.hpp>

const std::string CLASS_NAME {"[TaskQueue]"};

struct BasicTask : public Runnable {
    void run() {

    }
};

struct ValueTask : public Runnable {
    void run() {
        i = 5;
    }

    int i;
};

TEST_CASE("add-task", CLASS_NAME) {
    threading::TaskQueue queue;
    {
        std::shared_ptr task = std::make_shared<BasicTask>();
        queue.addTask(std::weak_ptr<Runnable>(task));
    }
    REQUIRE(queue.hasNextTask() == true);
    auto tasks = queue.getMultipleTasks(1);
    REQUIRE(tasks.size() == 1);
    REQUIRE(queue.hasNextTask() == false);
}

TEST_CASE("execute-task", CLASS_NAME) {
    threading::TaskQueue queue;
    std::shared_ptr<Runnable> task = std::make_shared<ValueTask>();

    queue.addTask(std::weak_ptr<Runnable>(task));
    REQUIRE(queue.hasNextTask() == true);

    auto tasks = queue.getMultipleTasks(1);
    tasks.at(0)->run();

    REQUIRE(dynamic_cast<ValueTask*>(tasks.at(0).get())->i == 5);
    REQUIRE(queue.hasNextTask() == false);
}

TEST_CASE("add-multiple-tasks", CLASS_NAME) {
    threading::TaskQueue queue;

    std::shared_ptr task = std::make_shared<BasicTask>();

    for (int i {0}; i < 10; i++) {
        queue.addTask(std::weak_ptr<Runnable>(task));
    }

    REQUIRE(queue.hasNextTask() == true);
    auto tasks = queue.getMultipleTasks(10);
    REQUIRE(tasks.size() == 10);
    REQUIRE(queue.hasNextTask() == false);
}
