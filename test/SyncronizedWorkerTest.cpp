#include <catch_amalgamated.hpp>

#include <string>

#include <Worker.hpp>
#include <Task.hpp>
#include <Governor.hpp>
#include <SyncronizedWorker.hpp>

const std::string CLASS_NAME {"[SynronizedWorker]"};

struct TestGovernor : public threading::Governor {
    std::string finishedWorkerId;
    void signalWorkerFinish(threading::Worker* worker) {
    }

    void signalWorkerStarted(threading::Worker* worker) {

    }

    void signalWorkerTaskCompleted(threading::Worker* worker) {
        finishedWorkerId = worker->id;
    }
};

TEST_CASE("syncronized-worker", CLASS_NAME) {
    int value = 0;
    auto increment = [&] {
        value += 5;
    };
    auto job = std::make_shared<threading::Task<decltype(increment)>>(increment);
    threading::SyncronizedWorker worker(nullptr);
    worker.assignTask(job);
    std::this_thread::sleep_for(std::chrono::nanoseconds(5));
    worker.work();
    worker.waitToCompleteTask();
    REQUIRE(value == 5);
}

TEST_CASE("syncronized-worker-governor", CLASS_NAME)  {
    auto governor = std::make_shared<TestGovernor>();
    auto func = [] {};
    auto job = std::make_shared<threading::Task<decltype(func)>>(func);
    threading::SyncronizedWorker worker(governor.get());
    worker.assignTask(job);
    worker.work();
    worker.waitToCompleteTask();
    REQUIRE(governor->finishedWorkerId == worker.id);
}

TEST_CASE("syncronized-worker-finished-add-task", CLASS_NAME) {
    int value = 0;
    auto increment = [&] {
        value += 5;
    };
    auto job = std::make_shared<threading::Task<decltype(increment)>>(increment);
    threading::SyncronizedWorker worker(nullptr);
    worker.work();
    worker.assignTask(job);
    worker.waitToCompleteTask();
    worker.stop();
    worker.assignTask(job);
    worker.waitToCompleteTask();
    REQUIRE(value == 5);
}

TEST_CASE("syncronized-worker-wait", CLASS_NAME) {
    int value = 0;
    auto increment = [&] {
        value += 5;
    };
    auto job = std::make_shared<threading::Task<decltype(increment)>>(increment);
    threading::SyncronizedWorker worker(nullptr);
    worker.work();
    worker.assignTask(job);
    worker.waitToCompleteTask();
    REQUIRE(value == 5);
    worker.assignTask(job);
    worker.waitToCompleteTask();
    REQUIRE(value == 10);
}
