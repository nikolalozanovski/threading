#include <Threadpool.hpp>

#include <algorithm>
#include <condition_variable>
#include <utility>
#include <functional>
#include <ranges>
#include <cstdint>
#include <chrono>

#include <UUID.hpp>
#include <SyncronizedWorker.hpp>

#include <stdexcept>

namespace threading {
    Threadpool::Threadpool(const size_t numberOfWorkers) :
        taskQueues{std::forward_list<TaskQueue>(THREAD_PRIORITY_QUEUE_COUNT)},
        taskDispatcher{this},
        threads{generateNumberOfSyncronizedWorkers(numberOfWorkers)},
        state(State::Working) {

        runningTasks.store(0);
        taskCount.store(0);

        taskDispatcher.work();
    };

    Threadpool::~Threadpool() {
        taskCount.store(0);
        stop();
        taskDispatcher.waitWorkerToFinish();
    }

    void Threadpool::addRunnable(std::weak_ptr<Runnable> runnable) {
        addTaskImpl(TASK_PRIORITY_NORMAL, runnable);
    }

    void Threadpool::addPrioritizedRunnable(uint64_t priority, std::weak_ptr<Runnable> runnable) {
        addTaskImpl(priority, runnable);
    }

    uint64_t Threadpool::numberOfTasks() noexcept {
        return taskCount;
    }

    bool Threadpool::waitTasks() {
        while (numberOfTasks() > 0 && hasWorkers()) {
            std::this_thread::sleep_for(std::chrono::nanoseconds(50));
        }

        return true;
    }

    uint64_t Threadpool::workerCount() noexcept {
        return threads.size();
    }

    void Threadpool::addTaskImpl(uint64_t priority, std::weak_ptr<Runnable> taskToAdd) {
        std::unique_lock ul(taskMutex);
        auto queueIterator = taskQueues.begin();
        std::advance(queueIterator, priority);
        queueIterator->addTask(taskToAdd);
        ul.unlock();
        taskCount++;
    }

    std::vector<std::shared_ptr<Runnable>> Threadpool::getNextTasks(uint64_t numberOfTasks) {
        std::unique_lock ul(taskMutex);
        uint64_t remainingTasks {[&] {
                uint64_t count = taskCount.load() - runningTasks.load();

                if (count < numberOfTasks  ) {
                    return count;
                }

                return numberOfTasks;
            }()};
        std::vector<std::shared_ptr<Runnable>> tasks;

        if (remainingTasks == 0) {
            return tasks;
        }

        for (auto& taskList : taskQueues) {
            if (!taskList.hasNextTask()) {
                continue;
            }

            auto nextTasks = taskList.getMultipleTasks(remainingTasks);
            tasks.insert(tasks.end(), nextTasks.begin(), nextTasks.end());
            remainingTasks -= tasks.size();
        }

        ul.unlock();
        return tasks;
    }

    std::unordered_map<std::string, std::shared_ptr<Syncronizeable>>
            Threadpool::generateNumberOfSyncronizedWorkers(
    uint64_t numberOfWorkers) {
        std::unordered_map<std::string, std::shared_ptr<Syncronizeable>> workers;
        std::generate_n(std::inserter(workers, std::begin(workers)), numberOfWorkers, [&] {
            const std::string uuid = UUID::generate();
            return std::make_pair(std::string(uuid),
            std::make_shared<SyncronizedWorker>(&taskDispatcher, std::string(uuid)));
        });
        return workers;
    }

    bool TaskQueue::hasNextTask() {
        return !queue.empty();
    }

    bool Threadpool::hasUnscheduledTasks() {
        return taskCount.load() > runningTasks.load();
    }

    bool Threadpool::hasWorkers() {
        return threads.size() > 0;
    }

    void Threadpool::pause() {
        state = State::Paused;
        taskDispatcher.block();
        taskDispatcher.pauseAllWorkers();
    }

    void Threadpool::resume() {
        state = State::Working;
        taskDispatcher.resumeAllWorkers();
        taskDispatcher.work();
    }

    void Threadpool::stop() {
        state = State::Stopped;
        taskDispatcher.stopAllWorkers();
    }


    bool Threadpool::isStopped() {
        return state == State::Stopped;
    }

    bool Threadpool::isWorking() {
        return state == State::Working;
    }

    bool Threadpool::isPaused() {
        return state == State::Paused;
    }
}
