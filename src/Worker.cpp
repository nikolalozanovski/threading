#include <Worker.hpp>

#include <UUID.hpp>

namespace threading {

    Worker::Worker() : id(UUID::generate()),
        thread (std::jthread(&Worker::run, this)) {}

    Worker::Worker(const std::string&& id) : id(std::move(id)),
        thread (std::jthread(&Worker::run, this)) {}

    Worker::~Worker() {
        stop();
        cv.notify_one();
    }

    void Worker::waitWorkerToFinish() {
        thread.join();
    }

    bool Worker::finishable() {
        return this->thread.joinable();
    }

    void Worker::pending() {
        std::lock_guard l(stateChangeMutex);

        if (!ableToWork()) {return;}

        state = Pending;
    }

    void Worker::running() {
        std::lock_guard l(stateChangeMutex);

        if (!ableToWork()) {return;}

        state = Running;
    }

    void Worker::finished() {
        std::lock_guard l(stateChangeMutex);

        if (!isFinished()) {return;}

        state = Finished;
    }

    void Worker::block() {
        std::lock_guard l(stateChangeMutex);

        if (!ableToWork()) {return;}

        state = Blocked;
    }

    void Worker::stop() {
        std::lock_guard l(stateChangeMutex);

        if (!ableToWork()) {return;}

        state = Stopping;
        cv.notify_one();
    }

    void Worker::detach() {
        this->thread.detach();
    }

    bool Worker::waitToCompleteTask() {
        if (isFinished() || isStopping()) {
            return false;
        }

        awaited = true;
        std::mutex mutexForWaiting;
        std::unique_lock lock(mutexForWaiting);
        cvForWaitingRecepients.wait(lock, [&] {
            return awaited == false;
        });
        return true;
    }

    bool Worker::isStopping() {
        std::lock_guard l(stateChangeMutex);
        return state == Stopping;
    }

    bool Worker::isRunning() {
        std::lock_guard l(stateChangeMutex);
        return state == Running;
    }

    bool Worker::isPending() {
        std::lock_guard l(stateChangeMutex);
        return state == Pending;
    }

    bool Worker::isBlocked() {
        std::lock_guard l(stateChangeMutex);
        return state == Blocked;
    }

    bool Worker::isFinished() {
        std::lock_guard l(stateChangeMutex);
        return state == Finished;
    }

    void Worker::work() {
        running();
        cv.notify_one();
    }

    void Worker::onStart() {
    }

    void Worker::onFinish() {
    }
    
    void Worker::onBeforeTask() {
    }

    void Worker::onAfterTask() {
    }

    void Worker::run() {
        std::mutex mutex;

        onStart();
        while (!isFinished()) {
            std::unique_lock ul (mutex);

            cv.wait(ul, [&] {
                return isStopping() or (not isBlocked() and shouldContinueWorking());
            });

            if (!ableToWork()) {
                break;
            }

            running();
            onBeforeTask();
            doTask();
            onAfterTask();
            pending();

            if (awaited) {
                releaseAllWaitingRecepients();
            }
        }

        finished();
        onFinish();
    }

    void Worker::releaseAllWaitingRecepients() {
        awaited = false;
        cvForWaitingRecepients.notify_all();
    }

    bool Worker::ableToWork() {
        std::lock_guard l(stateChangeMutex);
        return !(isStopping() || isFinished());
    }

    bool Worker::shouldContinueWorking() {
        return true;
    }
}
