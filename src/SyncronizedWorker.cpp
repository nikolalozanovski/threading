#include <SyncronizedWorker.hpp>

namespace threading {
    SyncronizedWorker::SyncronizedWorker(Governor* governor) : Worker(), governor(governor) {}

    SyncronizedWorker::SyncronizedWorker(Governor* governor, const std::string&& id)
        : Worker(std::move(id)), governor(governor) {}

    void SyncronizedWorker::doTask() {
        task->run();
    }

    bool SyncronizedWorker::assignTask(std::shared_ptr<Runnable>&& task) {
        if (not this->task) {
            this->task = std::move(task);
            wakeWorker();
            return true;
        }

        return false;
    }

    void SyncronizedWorker::SyncronizedWorker::work() {
        if (not shouldContinueWorking()) {
            pending();
            wakeWorker();
            return;
        }

        Worker::work();
    }

    void SyncronizedWorker::wakeWorker() {
        cv.notify_one();
    }

    void SyncronizedWorker::onStart() {
        if (governor) {
            governor->signalWorkerStarted(this);
        }
    }

    void SyncronizedWorker::onFinish() {
        if (governor) {
            governor->signalWorkerFinish(this);
        }
    }

    void SyncronizedWorker::onBeforeTask() {

    }

    void SyncronizedWorker::onAfterTask() {
        task.reset();

        if (governor) {
            governor->signalWorkerTaskCompleted(this);
        }
    }

    bool SyncronizedWorker::shouldContinueWorking() {
        return (bool)task;
    }
}
