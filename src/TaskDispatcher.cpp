#include <Threadpool.hpp>

#include <algorithm>
#include <ranges>

namespace threading {

    Threadpool::TaskDispatcher::TaskDispatcher(Threadpool* tp) :
        tp(tp) {
    }

    bool Threadpool::TaskDispatcher::hasFreeWorkers() {
        return !freeWorkers.empty();
    }

    void Threadpool::TaskDispatcher::signalWorkerFinish(Worker* worker) {
        std::unique_lock lock(syncronizationMutex);
        std::erase(freeWorkers, worker->id);
        tp->threads.erase(worker->id);
        dispatcherSynchronizator.notify_one();
    }

    void Threadpool::TaskDispatcher::signalWorkerStarted(Worker* worker) {
        std::unique_lock lock(syncronizationMutex);
        freeWorkers.push_back(worker->id);
        worker->work();
        dispatcherSynchronizator.notify_one();
    }

    void Threadpool::TaskDispatcher::signalWorkerTaskCompleted(Worker* worker) {
        std::unique_lock lock(syncronizationMutex);
        freeWorkers.push_back(worker->id);
        lock.unlock();
        tp->runningTasks--;
        tp->taskCount--;
        dispatcherSynchronizator.notify_one();
    }

    void Threadpool::TaskDispatcher::doTask() {

        if (not tp->isStopped()) [[likely]] {
            std::unique_lock lock(syncronizationMutex);
            dispatcherSynchronizator.wait_for(lock, std::chrono::nanoseconds(100), [&] {
                return (tp->hasUnscheduledTasks() && hasFreeWorkers());
            });

            auto nextTasks = tp->getNextTasks(freeWorkers.size());

            if (not nextTasks.empty()) {
                assignTasksToWorkers(nextTasks);

            } else {
                wakeWorkingWorkers();
            }

        } else [[unlikely]] {
            stopAllWorkersAndWaitToFinish();
        }
    }

    void Threadpool::TaskDispatcher::pauseAllWorkers() {
        if (tp->isStopped()) {
            return;
        }

        for (auto& thread : tp->threads) {
            auto workerPtr = dynamic_cast<Worker*>(thread.second.get());

            if (workerPtr->isFinished() or workerPtr->isStopping()) {
                continue;
            }

            workerPtr->block();
        }
    }

    void Threadpool::TaskDispatcher::resumeAllWorkers() {
        if (tp->isStopped()) {
            return;
        }

        for (auto& thread : tp->threads) {
            auto workerPtr = dynamic_cast<Worker*>(thread.second.get());

            if (workerPtr->isBlocked()) {
                workerPtr->work();
            }
        }
    }

    void Threadpool::TaskDispatcher::stopAllWorkers() {
        stop();
    }

    void Threadpool::TaskDispatcher::assignTasksToWorkers(std::vector<std::shared_ptr<Runnable>>& tasks) {
        for (auto& task : tasks) {
            tp->runningTasks++;
            tp->threads.at(freeWorkers.front())->assignTask(std::move(std::shared_ptr(task)));
            freeWorkers.pop_front();
        }
    }

    void Threadpool::TaskDispatcher::wakeWorkingWorkers() {
        std::ranges::for_each(tp->threads | std::views::filter([&](const auto & pair) {
            return std::find(freeWorkers.begin(), freeWorkers.end(), pair.first) == freeWorkers.end();
        }) | std::views::values, [&] (auto & workerPtr) {
            workerPtr->wakeWorker();
        });
    }

    void Threadpool::TaskDispatcher::stopAllWorkersAndWaitToFinish() {
        for (auto& worker : tp->threads) {
            auto workerPtr = dynamic_cast<Worker*>(worker.second.get());
            workerPtr->stop();

            if (workerPtr->finishable()) {
                worker.second->wakeWorker();
                workerPtr->waitWorkerToFinish();
            }
        }
    }
}
