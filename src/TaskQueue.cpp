#include <TaskQueue.hpp>

#include <ranges>
#include <algorithm>

namespace threading {
    void TaskQueue::addTask(std::weak_ptr<Runnable> task) {
        queue.emplace_back(task);
    }

    std::vector<std::shared_ptr<Runnable>> TaskQueue::getMultipleTasks(uint64_t numberOfTasks) {
        if (queue.empty()) {
            return std::vector<std::shared_ptr<Runnable>>();
        }

        auto currentTaskListSize = queue.size();

        if (currentTaskListSize < numberOfTasks) {
            return getNextTasks(currentTaskListSize);
        }

        return getNextTasks(numberOfTasks);
    }

    std::vector<std::shared_ptr<Runnable>> TaskQueue::getNextTasks(uint64_t numberOfTasks) {
        std::vector<std::shared_ptr<Runnable>> tasks;
        std::ranges::for_each(queue | std::ranges::views::take(numberOfTasks), [&](auto task) {
            tasks.emplace_back(task);
            queue.pop_front();
        });
        return tasks;
    }
}
