#pragma once

#include <memory>

#include <Runnable.hpp>

namespace threading {
    struct Syncronizeable {
        virtual bool assignTask(std::shared_ptr<Runnable>&& task) = 0;
        virtual void wakeWorker() = 0;
    };
}
