#pragma once
#include <functional>
#include <memory>
#include <type_traits>

#include <Callable.hpp>
#include <Runnable.hpp>
#include <NonVoidFunction.hpp>
#include <Completeable.hpp>

namespace threading {

    template <typename Callable, typename... Arguments>
    class Task : public Runnable, public Completeable<void> {
        public:
            Task(Callable callable, Arguments... args) : runnableTask{callable, args...} {}
            Task(Task& t) = delete;
            ~Task() = default;

            void run() {
                runnableTask.call();

                if (postRunnable) {
                    postRunnable();
                }
            }

            void onComplete(std::function<void()> postRunnable) {
                this->postRunnable = postRunnable;
            }
        protected:
        private:
            CallableTask<Callable, Arguments...> runnableTask;
            std::function<void()> postRunnable;
    };

    template <NonVoidFunction Callable, typename... Arguments>
    class Task<Callable, Arguments...> : public Runnable,
        public Completeable<std::invoke_result_t<Callable, Arguments...>> {
        public:

            Task(Callable callable, Arguments... args) : callableTask{callable, args...} {}
            Task(Task& t) = delete;
            ~Task() = default;

            void run() {
                auto result {callableTask.apply()};

                if (postRunnable) {
                    postRunnable(result);
                }
            }

            void onComplete(std::function<void(std::invoke_result_t<Callable, Arguments...>)> postRunnable) {
                this->postRunnable = postRunnable;
            }
        protected:
        private:
            CallableTask<Callable, Arguments...> callableTask;
            std::function<void(std::invoke_result_t<Callable, Arguments...>)> postRunnable;
    };
}
