#pragma once
#include <vector>
#include <memory>
#include <deque>

#include <Runnable.hpp>

namespace threading {

    class TaskQueue {
        public:
            void addTask(std::weak_ptr<Runnable> task);
            std::vector<std::shared_ptr<Runnable>> getMultipleTasks(uint64_t numberOfTasks);
            bool hasNextTask();
        protected:
        private:
            std::vector<std::shared_ptr<Runnable>> getNextTasks(uint64_t numberOfTasks);
            std::deque<std::shared_ptr<Runnable>> queue;
    };
}
