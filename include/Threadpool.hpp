#pragma once

#include <vector>
#include <deque>
#include <mutex>
#include <forward_list>
#include <atomic>
#include <functional>
#include <memory>
#include <unordered_set>
#include <unordered_map>

#include <Worker.hpp>
#include <Task.hpp>
#include <ReturnTypeOf.hpp>
#include <Completeable.hpp>
#include <Runnable.hpp>
#include <TaskQueue.hpp>
#include <Governor.hpp>
#include <Syncronizeable.hpp>

namespace threading {

    class Threadpool final {
        public:
            enum class State {
                Working,
                Paused,
                Stopped
            };

            const static int THREAD_PRIORITY_QUEUE_COUNT {5};

            const static int TASK_PRIORITY_CRITICAL {0};
            const static int TASK_PRIORITY_HIGH {1};
            const static int TASK_PRIORITY_MEDIUM {2};
            const static int TASK_PRIORITY_NORMAL {3};
            const static int TASK_PRIORITY_LOW {4};

            Threadpool(const size_t numberOfWorkers = 3);
            ~Threadpool();

            template <typename Callable, typename... Arguments>
            std::shared_ptr<Completeable<std::invoke_result_t<Callable, Arguments...>>> add(Callable callable,
                    Arguments... arguments) {
                return addPrioritizedTask(TASK_PRIORITY_NORMAL, callable, arguments...);
            }

            template <typename Callable, typename... Arguments>
            std::shared_ptr<Completeable<std::invoke_result_t<Callable, Arguments...>>> addPrioritizedTask(
                uint64_t priority,
                Callable callable, Arguments... arguments) {
                std::shared_ptr<Task<Callable, Arguments...>> taskToAdd(
                            new Task<Callable, Arguments...>(callable, arguments...));
                addTaskImpl(priority, std::weak_ptr<Runnable>(taskToAdd));
                return taskToAdd;
            }

            void addRunnable(std::weak_ptr<Runnable> runnable);
            void addPrioritizedRunnable(uint64_t priority, std::weak_ptr<Runnable> runnable);

            uint64_t numberOfTasks() noexcept;
            bool waitTasks();
            uint64_t workerCount() noexcept;

            void pause();
            void resume();
            void stop();

            bool isStopped();
            bool isWorking();
            bool isPaused();
        protected:
        private:
            struct TaskDispatcher final : public Governor, public Worker  {
                void doTask();

                TaskDispatcher(Threadpool* tp);

                TaskDispatcher(TaskDispatcher&) = delete;
                TaskDispatcher(TaskDispatcher&&) = delete;

                bool hasFreeWorkers();
                void signalWorkerFinish(Worker* worker);
                void signalWorkerStarted(Worker* worker);
                void signalWorkerTaskCompleted(Worker* worker);
                /**
                 * Pause the execution of all workers with the exception of those working executing a task.
                 * Only running and pending workers are eligible to be paused.
                 * See @stop for more details.
                 **/
                void pauseAllWorkers();

                /**
                 * Resume all paused workers. All workers that are executing a task will be able to finish the task.
                 **/
                void resumeAllWorkers();

                /**
                 * Stop all workers from execution. All workers that are executing a task will be stopped after
                 * the workers finish the tasks independently.
                 * This action is non reversible.
                 **/
                void stopAllWorkers();

                void assignTasksToWorkers(std::vector<std::shared_ptr<Runnable>>& tasks);
                void wakeWorkingWorkers();
                void stopAllWorkersAndWaitToFinish();

                Threadpool* tp;
                std::condition_variable dispatcherSynchronizator;
                std::deque<std::string> freeWorkers;
                std::mutex syncronizationMutex;
            };

            bool hasUnscheduledTasks();
            bool hasWorkers();
            void addTaskImpl(uint64_t priority, std::weak_ptr<Runnable> taskToAdd);
            std::vector<std::shared_ptr<Runnable>> getNextTasks(uint64_t count);
            std::unordered_map<std::string, std::shared_ptr<Syncronizeable>> generateNumberOfSyncronizedWorkers(
                        uint64_t numberOfWorkers);

            //how many threads are active
            std::forward_list<TaskQueue> taskQueues;
            TaskDispatcher taskDispatcher;
            std::unordered_map<std::string, std::shared_ptr<Syncronizeable>> threads;

            std::mutex taskMutex;
            std::atomic<std::uint64_t> runningTasks;
            std::atomic<std::uint64_t> taskCount;
            State state;
    };
}
