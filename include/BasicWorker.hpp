#pragma once

#include <memory>
#include <type_traits>

#include <Runnable.hpp>
#include <Task.hpp>
#include <Worker.hpp>

namespace threading {

    /**
    * Worker suitable for running the same task over and over again.
    **/
    class BasicWorker : public Worker {
        public:
            template <typename T, typename... Args>
            explicit BasicWorker(T func, Args... args) : task{std::make_unique<Task<T, Args...>>(func, args...)} {
                static_assert(std::is_invocable_v<T, Args...>,
                              "Unable to execute given function because is not invocable");
            }
        private:
            void doTask() {
                task->run();
            }
            std::unique_ptr<Runnable> task;
    };
}
