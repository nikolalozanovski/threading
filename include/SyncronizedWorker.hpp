#pragma once

#include <string>
#include <memory>

#include <Syncronizeable.hpp>
#include <Governor.hpp>
#include <Worker.hpp>

namespace threading {
    /**
    * This kind of worker is used for multiple tasks. The worker stop when there is no task assigned.
    * Use assignTask from @Syncronizeable to assign task to the worker
    */
    class SyncronizedWorker final : public Worker, public Syncronizeable {
        public:
            SyncronizedWorker(Governor* governor);
            SyncronizedWorker(Governor* governor, const std::string&& id);
            bool assignTask(std::shared_ptr<Runnable>&& task);
            void work();
            void wakeWorker();
        protected:
            void onStart();
            void onFinish();
            void onBeforeTask();
            void onAfterTask();
        private:
            void doTask();
            bool shouldContinueWorking();

            std::shared_ptr<Runnable> task;
            Governor* governor;
    };
}
