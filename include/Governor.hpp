#pragma once

#include <Worker.hpp>

namespace threading {
    struct Governor {
        virtual void signalWorkerFinish(Worker* worker) = 0;
        virtual void signalWorkerStarted(Worker* worker) = 0;
        virtual void signalWorkerTaskCompleted(Worker* worker) = 0;
    };
}
