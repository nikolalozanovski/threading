#pragma once

#include <functional>

template <typename Result>
class Completeable {
    public:
        virtual void onComplete(std::function<void(Result)> function) = 0;
    protected:
    private:
};

template<>
class Completeable<void> {
    public:
        virtual void onComplete(std::function<void()> function) = 0;
    protected:
    private:
};
