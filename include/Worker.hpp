#pragma once

#include <type_traits>
#include <thread>
#include <mutex>
#include <memory>
#include <condition_variable>
#include <functional>
#include <string>

namespace threading {
    class Worker {
        public:
            enum State {
                /**
                 * Indicates that the worker has finished and is in process of running
                 * other or the same task or waiting for other task.
                 **/
                Pending,
                /**
                 * Indicates that the worker has started working the task.
                 **/
                Running,
                /**
                 * Indicates that the worker has finished execution and no additional
                 * task will be executed.
                 **/
                Finished,
                /**
                 * Indicates that the worker has been blocked from execution and may continue
                 * execution after it's state has been moved from Blocked.
                 **/
                Blocked,
                /**
                 * Indicates that the worker is in process of finishing it's execution, later
                 * will become Finished and no task will be executable after that.
                 **/
                Stopping
            };

            explicit Worker();
            explicit Worker(const std::string&& id);
            virtual ~Worker();

            /**
             * Calling this function is dangerous as it's possible to hang the calling thread
             * It should be called on if #isStopping() returns true.
             **/
            void waitWorkerToFinish();
            bool finishable();

            void pending();
            void running();
            void finished();
            void block();
            void stop();
            // todo validate if detach is useful
            void detach();
            /**
             * Wait for task to complete only if the task has been started
             **/
            bool waitToCompleteTask();
            bool isStopping();
            bool isRunning();
            bool isPending();
            bool isBlocked();
            bool isFinished();

            void work();

            const std::string id;
        protected:
            std::condition_variable cv;
            virtual bool shouldContinueWorking();

            virtual void onStart();
            virtual void onFinish();
            virtual void onBeforeTask();
            virtual void onAfterTask();
        private:
            virtual void doTask() = 0;
            void run();
            void releaseAllWaitingRecepients();
            bool ableToWork();

            State state = State::Blocked;
            std::recursive_mutex stateChangeMutex;
            std::jthread thread;
            bool awaited;
            std::condition_variable cvForWaitingRecepients;
    };
}
